document.getElementById("Calcular").addEventListener("click", function() {
    const cantidad = parseFloat(document.getElementById("txtCantidad").value);
    const conversion = document.querySelector('input[name="conversion"]:checked').value;
    let resultado = 0;

    if (conversion === "CelsiusAFahrenheit") {
        resultado = (cantidad * 9/5) + 32;
    } else if (conversion === "FahrenheitACelsius") {
        resultado = (cantidad - 32) * 5/9;
    }

    document.getElementById("resultado").textContent = resultado.toFixed(2);
});

document.getElementById("Limpiar").addEventListener("click", function() {
    document.getElementById("txtCantidad").value = "";
    document.getElementById("resultado").textContent = "";
});