function generarEdades() {
    const edades = [];
    for (let i = 0; i < 100; i++) {
        edades.push(Math.floor(Math.random() * 91));
    }
    
    let bebes = 0;
    let ninos = 0;
    let adolescentes = 0;
    let adultos = 0;
    let ancianos = 0;

    for (let i = 0; i < edades.length; i++) {
        const edad = edades[i];
        if (edad >= 1 && edad <= 3) {
            bebes++;
        } else if (edad >= 4 && edad <= 12) {
            ninos++;
        } else if (edad >= 13 && edad <= 17) {
            adolescentes++;
        } else if (edad >= 18 && edad <= 60) {
            adultos++;
        } else if (edad >= 61 && edad <= 100) {
            ancianos++;
        }
    }

    document.getElementById('edades').textContent = "Edades de las 100 personas: " + edades.join(', ');
    document.getElementById('bebes').textContent = bebes;
    document.getElementById('ninos').textContent = ninos;
    document.getElementById('adolescentes').textContent = adolescentes;
    document.getElementById('adultos').textContent = adultos;
    document.getElementById('ancianos').textContent = ancianos;


    const sumaEdades = edades.reduce((total, edad) => total + edad, 0);
    const promedio = sumaEdades / edades.length;
    document.getElementById('promedio').textContent = "Promedio de edades: " + promedio.toFixed(2);
}


function limpiar() {
    document.getElementById("edades").textContent = "";
    document.getElementById("bebes").textContent = "";
    document.getElementById("ninos").textContent = "";
    document.getElementById("adolescentes").textContent = "";
    document.getElementById("adultos").textContent = "";
    document.getElementById("ancianos").textContent = "";
    document.getElementById("promedio").textContent = "";
}