function calcularCosto() {
    var precio = parseFloat(document.getElementById("precio").value);
    var tipoViaje = document.getElementById("tipoViaje").value;

    var subtotal = precio;
    var impuesto = subtotal * 0.16;

    if (tipoViaje === "doble") {
      subtotal *= 1.8; 
    }

    var totalPagar = subtotal + impuesto;

    document.getElementById("subtotal").innerHTML = "$" + subtotal.toFixed(2);
    document.getElementById("impuesto").innerHTML = "$" + impuesto.toFixed(2);
    document.getElementById("totalPagar").innerHTML = "$" + totalPagar.toFixed(2);
  }

  function limpiarFormulario() {
    document.getElementById("formularioBoleto").reset();
    document.getElementById("subtotal").innerHTML = "";
    document.getElementById("impuesto").innerHTML = "";
    document.getElementById("totalPagar").innerHTML = "";
  }